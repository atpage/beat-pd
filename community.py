
import pandas as pd

def load_syn_data(tablename, syn, download_to='/home/alex/code/beat-pd/community_data/'):
    entity = syn.tableQuery('select * from %s' % tablename)
    df = entity.asDataFrame()
    df = df.set_index('measurement_id')  # TODO: maybe ['subject_id','measurement_id']
    accel_cols = [c for c in df.columns if 'accelerometer' in c]
    for col in accel_cols:
        file_map = syn.downloadTableColumns(
            entity,
            col,
            #downloadLocation = download_to  # this doesn't seem to do anything
        )
        df[col] = [file_map[str(int(x))] if ((not pd.isnull(x)) and (str(int(x)) in file_map)) else None for x in df[col]]
    return df
