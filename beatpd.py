
################################### Imports: ###################################

import os
import glob
from warnings import warn

import numpy as np
import pandas as pd
import progressbar
from sklearn.base import is_classifier, is_regressor

from afe import AFE
from ml import prep_data, get_roc_values, get_top_features, get_mode

#################################### Setup: ####################################

print_debug = True

def debug(message):
    # TODO: make this controllable from outside
    if print_debug:
        print(message)

################################# Main class: ##################################

class BEATPD:
    def __init__(self, data_root='/home/alex/code/beat-pd/data/', studyname='CIS-PD'):
        """The data_root directory is assumed to look like:
            data/
            ├── BEAT-PD_Challenge_Data_Dictionary.csv
            ├── CIS-PD
            │   ├── cis-pd.CIS-PD_Test_Data_IDs.csv
            │   ├── CIS-PD_Demographics.csv
            │   ├── CIS-PD_Training_Data_IDs_Labels.csv
            │   ├── CIS-PD_UPDRS_Part1_2_4.csv
            │   ├── CIS-PD_UPDRS_Part3.csv
            │   ├── testing_data
            │   │   ├── 003ded56-c3d8-4d24-8dac-5a6a1c6b0943.csv
            │   │   ├── 0077eed2-5655-4352-a085-b4515f8f05e3.csv
            │   │   ├── ...
            │   └── training_data
            │       ├── 004ed441-24db-4839-8b5d-7465e4ea2a0a.csv
            │       ├── 00544f67-c07c-4a07-9c17-a7aee51d8b96.csv
            │       ├── ...
            └── REAL-PD
                ├── REAL-PD_Demographics.csv
                ├── real-pd.REAL-PD_Test_Data_IDs.csv
                ├── REAL-PD_Smartphone_Metadata.csv
                ├── REAL-PD_Training_Data_IDs_Labels.csv
                ├── REAL-PD_UPDRS_Part1_2_4.csv
                ├── REAL-PD_UPDRS_Part3.csv
                ├── testing_data
                │   ├── smartphone_accelerometer
                │   │   ├── 05137359-a055-489a-96e9-627501715140.csv
                │   │   ├── 052a204e-429c-4a8a-9baf-f53e11b3e504.csv
                │   │   ├── ...
                │   ├── smartwatch_accelerometer
                │   │   ├── 05137359-a055-489a-96e9-627501715140.csv
                │   │   ├── 052a204e-429c-4a8a-9baf-f53e11b3e504.csv
                │   │   ├── ...
                │   └── smartwatch_gyroscope
                │       ├── 05137359-a055-489a-96e9-627501715140.csv
                │       ├── 052a204e-429c-4a8a-9baf-f53e11b3e504.csv
                │       ├── ...
                └── training_data
                    ├── smartphone_accelerometer
                    │   ├── 00a49337-386c-4de3-a220-4cf3c0d20a7d.csv
                    │   ├── 00e19ae8-3da9-4e76-b9f6-3f565b3e5f06.csv
                    │   ├── ...
                    ├── smartwatch_accelerometer
                    │   ├── 00e19ae8-3da9-4e76-b9f6-3f565b3e5f06.csv
                    │   ├── 00f1d52e-03cf-4031-a616-9a66f961d994.csv
                    │   ├── ...
                    └── smartwatch_gyroscope
                        ├── 00e19ae8-3da9-4e76-b9f6-3f565b3e5f06.csv
                        ├── 00f1d52e-03cf-4031-a616-9a66f961d994.csv
                        ├── ...
        """
        self.studyname = studyname
        self.data_root = data_root
        self.demographics = load_demographics(data_root, studyname)
        self.labels = load_labels(data_root, studyname)
        self.updrs = load_updrs(data_root, studyname)
        self.top_features = {}
        # self.train_columns = {}

    def load_training_set(self, pattern='training_data/*.csv', filelist=None, fraction=0.8):
        """If fraction is not 1.0 (100%), some of the matching data will be
        diverted to the test set, stratified to include data from each
        participant and each set of labels for that participant.  You
        may specify either a pattern (glob) or an explicit list of files.

        recommended patterns:
             - 'training_data/*.csv'                  # for CIS-PD training set
             - 'training_data/*_accelerometer/*.csv'  # for REAL-PD training set, without gyro
        """
        if fraction > 1 or fraction < 0:
            raise ValueError("train/test split fraction must be between 0 and 1.")
        if filelist is None:
            recordings = glob.glob(os.path.join(self.data_root,self.studyname,pattern))
        else:
            recordings = filelist
        if fraction != 1:
            # Take a fraction from each subject for train + test.
            recs_by_subj = {}
            for rec in recordings:
                measurement_id = os.path.splitext(os.path.basename(rec))[0]
                labels_row = self.labels.loc[measurement_id]
                sid = labels_row.subject_id
                lbs = labels_row['off_meds'].astype(str)+','+\
                      labels_row['dyskinesia'].astype(str)+','+\
                      labels_row['tremor'].astype(str)  # e.g. '2.0,nan,1.0'
                if sid not in recs_by_subj:
                    recs_by_subj[sid] = {}
                if lbs not in recs_by_subj[sid]:
                    recs_by_subj[sid][lbs] = []
                recs_by_subj[sid][lbs].append(rec)
            train_recs = []
            test_recs = []
            for sid in recs_by_subj:
                for lbs in recs_by_subj[sid]:
                    # Recordings with rare labels are randomly
                    # assigned to training or testing (i.e. 50/50,
                    # ignoring the `fraction` parameter).
                    num_recs = len(recs_by_subj[sid][lbs])
                    shuffled_recs = list(np.random.permutation(recs_by_subj[sid][lbs]))
                    if num_recs == 1:
                        for_training = np.random.randint(0,2)
                    elif num_recs == 2:
                        for_training = 1
                    else:
                        for_training = round(fraction*num_recs)
                    train_recs += shuffled_recs[:for_training]
                    test_recs  += shuffled_recs[for_training:]
        else:
            train_recs = recordings
            test_recs  = []
        if len(train_recs):
            self.train_recs = train_recs
            self.training_data, self.training_labels = self.extract_features(train_recs)
            self.clean_training_data()
        if len(test_recs):
            self.test_recs = test_recs
            self.test_data,     self.test_labels     = self.extract_features(test_recs)

    def load_test_set(self, pattern='training_data/*.csv', filelist=None, test_recs_df=None):
        """pattern is ignored if you manually specify the list of test_recs
        (as a dataframe including meas+subj IDs, or as a filelist).
        otherwise, recommended patterns are:
             - 'testing_data/*.csv'                   # for CIS-PD test set
             - 'testing_data/*_accelerometer/*.csv'   # for REAL-PD test set, without gyro
        """
        phone_recs = []
        watch_recs = []
        if test_recs_df is None:
            if filelist is None:
                test_recs = glob.glob(os.path.join(self.data_root,self.studyname,pattern))
            else:
                test_recs = filelist
        else:
            # This block handles stuff from the Community Phase of the
            # challenge.  test_recs_df looks like:
            #                                        subject_id  smartphone_accelerometer  smartwatch_accelerometer
            #   measurement_id
            #   68094ac9-3881-4ac2-9359-e671f51164f4     hbv012         /path/to/file.csv         /path/to/file.csv
            #   39493722-71a0-4ea7-892f-cf7bc8560d5d     hbv013         /path/to/file.csv         /path/to/file.csv
            #   ...
            # we're adding some extra columns (sid+filename) to
            # self.labels to pass this info around, as well as the
            # phone_recs/watch_recs arrays.
            accel_cols = [c for c in test_recs_df.columns if 'accelerometer' in c]
            test_recs = []
            sids = []
            meas_ids = []
            for col in accel_cols:
                has_filename = ~pd.isnull(test_recs_df[col])
                filenames = list( test_recs_df.loc[has_filename,col].values )
                test_recs += filenames
                sids      += list( test_recs_df.loc[has_filename,'subject_id'].values )
                meas_ids  += list( test_recs_df[has_filename].index.values )
                if 'phone' in col:
                    phone_recs += filenames
                elif 'watch' in col:
                    watch_recs += filenames
            df = pd.DataFrame(
                index = pd.Index(data=meas_ids, name='measurement_id'),
                columns = ['subject_id', 'filename', 'off_meds', 'dyskinesia', 'tremor']
            )
            df['subject_id'] = sids
            df['filename'] = test_recs
            df.loc[:, ['off_meds', 'dyskinesia', 'tremor']] = \
                (np.nan, np.nan, np.nan)
            df = df[df['subject_id'].isin(self.demographics.index.values)]  # drop pts we don't know about.
            # TODO?: also drop ones with missing UPDRS
            self.labels = self.labels[~self.labels.index.isin(meas_ids)]
            self.labels = self.labels.append(df)
        if len(test_recs):
            self.test_recs = test_recs
            self.test_data, self.test_labels = self.extract_features(test_recs, phone_recs=phone_recs, watch_recs=watch_recs)

    def clean_training_data(self):
        """remove useless columns from training data"""
        to_drop = self.training_data.nunique() <= 1
        # TODO: could also consider sklearn.feature_selection.VarianceThreshold
        to_drop = to_drop[to_drop==True].index
        to_drop = [col for col in to_drop if col!='subject_id']  # keep sid for now.  may remove this later.
        self.training_data = self.training_data.drop(columns=to_drop)

    def extract_features(self, recordings, window_size=30, phone_recs=[], watch_recs=[], breakage_mode=False):
        """recordings is a list of files to extract features from.  window
        size is seconds per window for AFE.  phone_recs and watch_recs
        are used to look up whether a recording is from a phone or a
        watch, if we can't do it by the filename.

        breakage_mode will ignore device id in data that has it,
        simulating the bug that existed during the contest phase.
        """
        debug("Extracting features...")
        data = []
        labels = []
        for recording in progressbar.progressbar(recordings):  # TODO?: parallelize this loop
            measurement_id = os.path.splitext(os.path.basename(recording))[0]
            if measurement_id not in self.labels.index:
                if ('filename' in self.labels.columns) and (recording in self.labels['filename'].values):
                    measurement_id = self.labels.index[self.labels['filename']==recording][0]
                else:
                    warn("Skipping file %s; not found in labels." % measurement_id)
                    continue
            subject_id = self.labels.loc[measurement_id].subject_id
            if type(subject_id) != str and type(subject_id) != np.int64 and type(subject_id) != np.float64 and \
               len(subject_id) > 1:
                subject_id = list(set(subject_id))
                if len(subject_id) > 1:
                    raise RuntimeError("Couldn't find unique subjid for %s" % recording)
                else:
                    subject_id = subject_id[0]
            # print("working on file %s from subj %s" % (recording, subject_id))
            accel = pd.read_csv(recording, index_col=0)
            accel = accel.rename(columns={'X':'x','Y':'y','Z':'z'})
            if breakage_mode or ('device_id' not in accel.columns):
                device_ids = [None]
            else:
                device_ids = accel['device_id'].unique()
            ft_dfs = []
            for did in device_ids:
                if did is not None:
                    accel_slice = accel[ accel['device_id'] == did ]
                else:
                    if len(device_ids) != 1:
                        raise ValueError("If there are multiple device IDs, none of them should be NULL.")
                    accel_slice = accel
                try:
                    afe = AFE(accel_slice, warn_interpolating=False)
                except:
                    print(recording, "failed")
                    raise
                ft = afe.get_features(window_size=window_size)  # TODO: fmin, fmax
                ft['device_id'] = np.full(len(ft), did)
                ft = ft.set_index('device_id', append=True)
                ft_dfs.append(ft)
            ft = pd.concat(ft_dfs, sort=False)
            # ft is now a dataframe with ~16 columns of features, and [epoch_start, device_id] as the index
            ft['measurement_id'] = np.full(len(ft), measurement_id)
            #ft['device_id'] = np.full(len(ft), first_device_id)
            ft['subject_id'] = np.full(len(ft), subject_id)
            #ft['study']      = np.full(len(ft), self.studyname)  # TODO: onehot with studyname
            ft['age']        = np.full(len(ft), self.demographics.loc[subject_id].Age)
            ft['gender']     = np.full(len(ft), self.demographics.loc[subject_id].Gender)
            if self.studyname == 'CIS-PD' or 'smartwatch' in recording or recording in watch_recs:
                ft['watch'] = np.full(len(ft), True)
                ft['phone'] = np.full(len(ft), False)
            elif 'smartphone' in recording or recording in phone_recs:
                # TODO: need to get this info from community phase data
                ft['watch'] = np.full(len(ft), False)
                ft['phone'] = np.full(len(ft), True)
            else:
                # TODO: getting in here is a problem; likely to result
                # in NaN columns for these features.
                ft['watch'] = np.full(len(ft), None)
                ft['phone'] = np.full(len(ft), None)
            # Add a 'sensor' metadata column to the index.  This is
            # redundant, but needed for community phase:
            ft['sensor'] = np.full(len(ft), '', dtype=object)
            ft.loc[ft['phone'],'sensor'] = 'smartphone_accelerometer'
            ft.loc[ft['watch'],'sensor'] = 'smartwatch_accelerometer'
            series = self.updrs['124'].loc[subject_id]
            for col in series.index:
                ft[col] = np.full(len(ft), series[col])
            for medstate in self.updrs['3'].index.unique(level=1):
                if pd.isnull(medstate):
                    continue  # TODO: maybe find a way to use this UPDRS info anyway.
                try:
                    series = self.updrs['3'].loc[(subject_id,medstate)]
                    for col in series.index:
                        ft[col+'_'+medstate+'Meds'] = np.full(len(ft), series[col])
                except KeyError:
                    continue
            this_meas_labels = self.labels.loc[measurement_id,['off_meds','dyskinesia','tremor']]
            # [self.labels.loc[measurement_id][col] for col in ['off_meds','dyskinesia','tremor']]
            if type(this_meas_labels) == pd.DataFrame:
                this_meas_labels = this_meas_labels.iloc[0]
            ft = ft.set_index('measurement_id', append=True)
            #ft = ft.set_index('device_id', append=True)
            ft = ft.set_index('sensor', append=True)
            ft = ft.reorder_levels(['measurement_id','epoch_start','sensor','device_id'])
            ft = ft.sort_index()  # TODO?
            lb = pd.DataFrame(
                data = np.full(
                    (len(ft),3),
                    this_meas_labels
                ),
                index = ft.index,
                columns = ['off_meds','dyskinesia','tremor'],
            )
            data.append(ft)
            labels.append(lb)
        # TODO: try block with concat below? (ValueError: No objects to concatenate)
        data   = pd.concat(data,   sort=False)
        labels = pd.concat(labels, sort=False)
        # fix yes/no column(s):
        for col in data.columns:
            if 'No' in data[col].values or 'Yes' in data[col].values:
                data[col].replace({'No': False, 'Yes': True}, inplace=True)  # TODO?:  0/1 vs true/false
        # and gender:
        if 'gender' in data.columns:
            data['gender'].replace({'Male': 1, 'Female': 0}, inplace=True)
            data.rename(columns={'gender': 'male'}, inplace=True)
        # Done.
        nsubj = len(data['subject_id'].unique())
        debug("Created dataset containing %d samples from %d participants." %
              (len(data), nsubj))
        #print(data.index,labels.index)  # ['measurement_id', 'epoch_start', 'sensor'], ['measurement_id', 'epoch_start']
        return data, labels

    # def init_clfs(self):
    #     self.clfs = []  # list of sklearn classifiers/regressors

    def train(self, measure, clf, thresh=None, do_feature_reduction=True, do_updrs_pca=True):
        """clf is a sklearn classifier/regressor.  thresh is used to threshold a measure
        for use with a binary classifier (rather than a regressor).
        """
        X_train, X_test, y_train, y_test = self.get_Xy_arrays(measure,
                                                              thresh=thresh,
                                                              clf=clf,
                                                              do_feature_reduction=do_feature_reduction,
                                                              do_updrs_pca=do_updrs_pca)
        # in case we want to test on something different later,
        # remember which columns to use on the new dataset:
        clf.features_used = X_train.columns
        clf.fit(X_train, y_train)
        debug("Trained clf for %s of %s." % (get_mode(clf), measure))
        #print("features were: %s" % str(X_train.columns))  # debugging

    def validate(self, measure, clf, thresh=None, plot=False, do_feature_reduction=True, do_updrs_pca=True):
        # if clf not in self.clfs:
        #     raise ValueError("That clf wasn't trained.")
        X_train, X_test, y_train, y_test = self.get_Xy_arrays(measure,
                                                              thresh=thresh,
                                                              clf=clf,
                                                              do_feature_reduction=do_feature_reduction,
                                                              do_updrs_pca=do_updrs_pca)
        # training rows without labels are removed earlier.  but test labels may
        # still be blank when we get here:
        X_test = X_test[~pd.isnull(y_test)]
        y_test = y_test[~pd.isnull(y_test)]
        if is_classifier(clf):
            measure = 'mean accuracy'
        if is_regressor(clf):
            measure = 'R^2'
        print("==== Results on training set: ====")
        train_score = clf.score(X_train, y_train)
        print("Score (%s): " % measure, train_score)
        if is_classifier(clf):
            fpr, tpr, threshes, auc = get_roc_values(clf, X_train, y_train)
            print("AUC: ", auc)
        print("==== Results on test set: ====")
        val_score = clf.score(X_test, y_test)
        print("Score (%s): " % measure, val_score)
        if is_classifier(clf):
            fpr, tpr, threshes, auc = get_roc_values(clf, X_test, y_test)
            print("AUC: ", auc)
        if plot:
            # TODO: call ml.whatever.  confusion matrix, etc.
            raise NotImplementedError()
        return train_score, val_score

    def get_Xy_arrays(self, measure, mode='regression', thresh=None,
                      clf=None, do_feature_reduction=True, do_updrs_pca=True):
        """Takes training+testing data and drops some rows and columns, makes some
        columns onehot, imputes+scales values, and ultimately yields X and y
        arrays (both train and test).

        measure is 'off_meds', 'dyskinesia', or 'tremor'.  mode is
        'classification' or 'regression'.  clf can override mode.

        In classification mode, the output measure will be binarized
        (>thresh vs <=thresh).
        """
        if clf is not None:
            mode = get_mode(clf)
        if not hasattr(self, 'training_data'):
            raise RuntimeError("Must load training data before getting X,y.")
        train_to_prep = self.training_data
        if not hasattr(self, 'test_data'):
            test_to_prep  = pd.DataFrame(columns=self.training_data.columns)
            test_labels   = pd.DataFrame(columns=self.training_labels.columns)
        else:
            test_to_prep  = self.test_data
            test_labels   = self.test_labels[measure]
        test_to_prep = unify_train_test_columns(train_to_prep, test_to_prep)  # TODO: unify on outside set...?
        if mode == 'classification':
            train_labels = self.training_labels[measure]>thresh
            test_labels = test_labels>thresh
        elif mode == 'regression':
            train_labels = self.training_labels[measure]
        else:
            raise ValueError("Invalid mode for data prep.")
        #print(train_to_prep.shape, test_to_prep.shape)  # debugging
        X_train, X_test, y_train, y_test = prep_data(
            train_to_prep,
            test_to_prep,
            train_labels,
            test_labels,
            do_pca=do_updrs_pca
        )
        if do_feature_reduction:
            if measure in self.top_features and mode in self.top_features[measure]:
                cols = self.top_features[measure][mode]
            else:
                cols = get_top_features(X_train, y_train, clf_mode=mode, clf=clf)
                if measure not in self.top_features:
                    self.top_features[measure] = {}
                self.top_features[measure][mode] = cols
            X_train = X_train[cols]
            if len(X_test) == 0:
                X_test = pd.DataFrame(index=X_test.index, columns=cols)
            else:
                X_test = X_test[cols]
        # if hasattr(self, 'train_columns') and (X_test.columns != self.train_columns):
        #     raise RuntimeError("Feature set has changed since clf ...")
        return X_train, X_test, y_train, y_test

    def predict_for_contest(self, measure, clf, print_error=True,
                            do_feature_reduction=True,
                            do_updrs_pca=True, logfile=None):
        """The DataFrame returned by this function is essentially the CSV that
        must be uploaded to Synapse.  But you would need to append two
        of them (two studies) into one CSV.
        """
        _, X_test, _, y_test = self.get_Xy_arrays(measure, clf=clf,
                                                  do_feature_reduction=do_feature_reduction,
                                                  do_updrs_pca=do_updrs_pca)
        if list(X_test.columns) != list(clf.features_used):
            #raise RuntimeError("clf was trained on different features (columns) than X_test")
            warn("clf was trained on different features (columns) than X_test.  attempting to reshape X_test.")
            # Add missing columns:
            for col in set(clf.features_used).difference(X_test.columns):
                if col.startswith('subject_'):
                    X_test[col] = False  # assume this pt was in training set but not test
                else:
                    X_test[col] = np.nan  # but this will raise a ValueError when we try to predict() below
            # Drop extra columns:
            X_test = X_test[list(clf.features_used)]
        #print("using these features for prediction: %s" % str(X_test.columns))  # debugging
        predictions = clf.predict(X_test)
        # basically doing manual groupby() below
        mids = self.test_data.index.get_level_values('measurement_id').unique().values
        output_idx = pd.Index(name='measurement_id', data=mids)
        output = pd.DataFrame(columns=['prediction'], index=output_idx)
        for measurement_id in mids:
            output.loc[measurement_id]['prediction'] = \
                np.mean(predictions[self.test_data.index.get_level_values('measurement_id')==measurement_id])
        if print_error:
            print_challenge_error(
                self.labels.loc[output.index],
                output['prediction'],
                measure = measure,
                logfile = logfile
            )
        return output

    def save_training_data(self, base_filename=None):
        """_MEASUREMENT.csv will be appended to base_filename"""
        if base_filename is None:
            base_filename = '%s_training_set' % self.studyname
        for col in self.training_labels.columns:
            data_for_disk = self.training_data.copy()
            data_for_disk[col] = self.training_labels[col]
            data_for_disk = data_for_disk[~pd.isnull(data_for_disk[col])]  # don't save NaN rows
            data_for_disk.to_csv(base_filename+'_%s.csv' % col, index=False)

############################## Helper functions: ###############################

def unify_train_test_columns(train, test):
    """make sure test has the same columns as train.  don't delete any columns from
    train."""
    delete_from_test = [c for c in test.columns if c not in train.columns]
    add_to_test = [c for c in train.columns if c not in test.columns]  # these will be NaN, and imputed later
    test = test.drop(columns=delete_from_test)
    for col in add_to_test:
        test[col] = np.full(len(test), np.nan)
    test = test[train.columns]  # ensure same order
    return test  # may not need to do this

def print_challenge_error(y_test, y_pred, measure=None, ignore_nan=True, logfile=None):
    """Prints the score (which is to be minimized).  y_test has (or may
    have) labels for all measures.  y_test must have 1 row per
    recording, and must contain the subject_id column.  e.g. it should
    be a subset of rows from the `labels` dataframe of a BEATPD
    object.  y_pred is the predictions for a single measure.  """
    if measure is None:
        raise ValueError("Must specify measure.")
    final_score = compute_challenge_error(
        y_test[measure],
        y_pred,
        pt_ids = y_test['subject_id'],
        ignore_nan = ignore_nan
    )
    print_str = "Challenge error for %s: %0.4f (using up to %d rows)" % \
        (measure, final_score, len(y_pred))
    if logfile is None:
        print(print_str)
    else:
        with open(logfile,'a') as f:
            f.write(print_str + "\n")

def compute_challenge_error(y_test, y_pred, **kwargs):
    """Computes the error based on the challenge definition (MSE weighted
    per-subject).  pt_ids is a list of participant IDs associated with
    the rows of y_test/y_pred.  y_test must be a series with index
    values that point back into the pt_ids array.  if ignore_nan is
    True, we will ignore NaNs in y_test.
    """
    ignore_nan = kwargs.get('ignore_nan', True)
    pt_ids = kwargs.get('pt_ids')
    pt_ids = pt_ids[y_test.index]
    if len(y_pred) != len(y_test):
        raise IndexError("Test and prediction arrays must be same length (got %d, %d)." % \
                         (len(y_test), len(y_pred)))
    if np.sum(pd.isnull(y_test)):
        if not ignore_nan:
            raise ValueError("Can't compute score with null labels.")
        else:
            # drop NaN rows
            nans = pd.isnull(y_test)
            y_test = y_test[~nans]
            y_pred = y_pred[~nans]
            pt_ids = pt_ids[~nans]
    final_score = 0
    sum_root_nks = 0
    errors = (y_test - y_pred)**2
    for subject_id in list(set(pt_ids)):
        # TODO: vectorize/groupby for this
        this_subject_observations = pt_ids==subject_id
        n_k = np.sum(this_subject_observations)
        this_subject_error = np.sum(errors[this_subject_observations]) / n_k
        final_score += n_k**0.5 * this_subject_error
        sum_root_nks += n_k**0.5
    final_score /= sum_root_nks
    return final_score

def load_demographics(data_root, studyname):
    demographics = pd.read_csv(
        data_root + '%s/%s_Demographics.csv' % (studyname,studyname),
        index_col = 'subject_id',
        na_values = ['Unknown'],
    )
    debug("Loaded demographic information for %d participants." % len(demographics.index.unique()))
    return demographics

def load_labels(data_root, studyname):  #, mode='training'):
    # Training labels:
    train_labels = pd.read_csv(
        data_root + '%s/%s_Training_Data_IDs_Labels.csv' % (studyname,studyname),
        index_col = 'measurement_id',
    )
    # on_off is a confusing header, since 0 is on and 1 or 4 are off:
    train_labels = train_labels.rename(columns={'on_off':'off_meds'})
    # Testing "labels":
    test_labels = pd.read_csv(
        data_root + '%s/%s.%s_Test_Data_IDs.csv' % (studyname,studyname.lower(),studyname),
        index_col = 'measurement_id',
    )
    for colname in ['off_meds','dyskinesia','tremor']:
        test_labels[colname] = np.full(len(test_labels), np.nan)
    # Merge them:
    labels = pd.concat([train_labels, test_labels], ignore_index=False)
    # # drop unlabeled rows:
    # labels = labels[
    #     ~pd.isnull(labels.off_meds) |
    #     ~pd.isnull(labels.dyskinesia) |
    #     ~pd.isnull(labels.tremor)
    # ]
    debug("Loaded labels for %d samples." % len(labels))
    return labels

def load_updrs(data_root, studyname):
    updrs = {}
    # Parts 1,2,4:
    updrs_124 = pd.read_csv(
        data_root + '%s/%s_UPDRS_Part1_2_4.csv' %  (studyname,studyname),
        index_col = 'subject_id',
    )
    updrs['124'] = updrs_124.drop(columns='Visit', errors='ignore')  # there's only 1 visit
    # Part 3:
    updrs_3 = pd.read_csv(
        data_root + '%s/%s_UPDRS_Part3.csv' % (studyname,studyname),
        index_col = ['subject_id', 'ParticipantState'],
    )
    updrs['3'] = updrs_3.drop(columns='Visit', errors='ignore')
    # Visit is covered well enough by ParticipantState
    updrs['3'].sort_index(inplace=True)
    updrs['3'].index.rename(['subject_id','meds'], inplace=True)
    debug("Loaded UPDRS scores.")
    return updrs

def predict(test_data, clf):
    """real-pd.REAL-PD_Test_Data_IDs.csv
         cis-pd.CIS-PD_Test_Data_IDs.csv"""
    raise NotImplementedError()

################################################################################
