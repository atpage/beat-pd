# BEAT-PD challenge #

This project contains code to analyze data from the BEAT-PD challenge.

### Prerequisites ###

You will need Python 3 with a few common libraries: sklearn, matplotlib, and progressbar2.  And of course numpy and pandas.

You will also need one of my other libraries, `AFE`, which is in [PyPI](https://pypi.org/project/AFE/) or [here](https://bitbucket.org/atpage/afe/).

Ensure that the machine running the code has sufficient RAM, e.g. 2+GB free.

### How do I run it? ###

Take a look at the Jupyter notebooks.  `experiments.ipynb` is used to test different machine learning models.  `contest.ipynb` is used to generate predictions for upload to Synapse.  The basic workflow is:

* Put the study data somewhere, following the directory structure shown in `BEATPD.__init__()`.

* Create a `BEATPD` object for one of the studies (CIS-PD or REAL-PD).  This
  object loads and stores the demographics, UPDRS scores, and PROs (i.e. labels)
  for the study.

* Use `load_training_set()` (and optionally `load_test_set()`) to extract
  features from all the CSV files.  This takes on the order of 10 minutes.

* You can now use `get_Xy_arrays()` to get `X_train`, `X_test`, `y_train`,
  `y_test` for use with your machine learning code.  There are also `train()`
  and `validate()` functions you can use without explicitly running
  `get_Xy_arrays()`.

Example:

    from beatpd import BEATPD
    from ml import make_error_boxplot

    from sklearn.ensemble import RandomForestRegressor

    data_root = '/home/alex/code/beat-pd/data/'

    cispd = BEATPD(data_root=data_root, studyname='CIS-PD')
    cispd.load_training_set(pattern='training_data/f*.csv')  # only load files starting with f, for a quick test

    clf = RandomForestRegressor(n_jobs=2)
    cispd.train(measure='tremor', clf=clf)

    # Print basic results:
    cispd.validate(measure='tremor', clf=clf)

    # Visualize results:
    X_train, X_test, y_train, y_test = cispd.get_Xy_arrays(measure='tremor', clf=clf)
    predictions = clf.predict(X_test)
    train_predictions = clf.predict(X_train)
    make_error_boxplot(
        y_predicted_test = predictions,
        y_test = y_test,
        featurename = 'tremor',
        y_predicted_train = train_predictions,
        y_train = y_train
    )

More information is available here: https://www.synapse.org/#!Synapse:syn21781334/wiki/602481

### Notes about data ###

CIS-PD pts have severe motor fluctuations.  They were given 6 months with Apple watch and iPhone questionnaires.  They rated their symptoms every 30 minutes for 48 hours before a visit.

REAL-PD pts had motor fluctuations too.  They are H&Y stage 2 mostly.  They used an android phone and watch.

UPDRS parts 1+2 are total score only.  Parts 3+4 are broken down.

Labels:

 * off_meds: This is True or False in REAL-PD.  0-4 in CIS-PD (4 = fully off, 1-3 = partially on, 0 = fully on).  Note that this is my label; in the source data the field is called "on_off" which is confusing since 0 means 'on'.
 * tremor: This is 0-4 (none->mild->...->severe) in both CIS-PD and REAL-PD.  However, nobody in REAL-PD reported a 4.
 * dyskinesia: 0-4 (none->mild->...->severe) in CIS-PD.  0-2 (none, non-troublesome, severe) in REAL-PD.

### Who do I talk to? ###

* Alex Page, alex.page@rochester.edu
