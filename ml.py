
import numbers
import warnings

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection   import train_test_split, GridSearchCV, RandomizedSearchCV
from sklearn.preprocessing     import StandardScaler
from sklearn.metrics           import roc_curve, roc_auc_score
from sklearn.exceptions        import DataConversionWarning
from sklearn.base              import is_classifier, is_regressor
from sklearn.ensemble          import RandomForestRegressor, RandomForestClassifier
from sklearn.feature_selection import RFECV, SelectKBest, mutual_info_regression, \
    mutual_info_classif  # f_regression, f_classif
from sklearn.decomposition     import PCA

pd.options.mode.chained_assignment = 'raise'

############################# Data prep functions: #############################

def is_numeric(column_name):
    if column_name in ['gender', 'watch', 'phone', 'study', 'measurement_id']:
        return False
    if column_name.startswith('subject_'):
        return False
    return True
    # TODO: any others that should or shouldn't be scaled?

def impute_simple(X_train, X_test):
    """Both X_train and X_test must be imputed based only on X_train."""
    # TODO: mean vs. median
    to_impute = [X_train, X_test]
    for df in to_impute:
        numeric_cols = [col for col in df.columns if is_numeric(col)]
        # we could consider using something other than mean (e.g. mode) to
        # impute non-numeric cols.
        for subject_id in df['subject_id'].unique():
            this_subj_rows = df['subject_id']==subject_id
            for col in numeric_cols:
                np_col = df[col].values
                needs_filled = pd.isnull(df[col])
                try:
                    # Try mean of this subject's measurements
                    if 'subject_id' in X_train.columns:
                        same_subj_rows = X_train['subject_id']==subject_id
                    else:
                        same_subj_rows = X_train['subject_'+get_sid_str(subject_id)]==True
                        # Note: that will break if we scale the bool columns.
                    avg_val = np.mean( X_train[col][same_subj_rows] )
                except ValueError:
                    avg_val = np.nan
                if pd.isnull(avg_val):
                    # Else use mean of whole population
                    avg_val = np.mean( X_train[col] )
                np_col[(this_subj_rows & needs_filled)] = avg_val

def impute_complex(X_train, X_test):
    """Both X_train and X_test must be imputed based only on X_train."""
    # TODO: this is a regression problem... what is the typical value of
    # each ft for a pt based on other fts?  could basically do recursive ML...
    raise NotImplementedError()

def impute(X_train, X_test, method='simple'):
    if method=='simple':
        return impute_simple(X_train, X_test)
    elif method=='complex':
        return impute_complex(X_train, X_test)
    else:
        raise ValueError("unknown imputation method")

def scale(X_train, X_test):
    """Scale all train+test values based on training set stats, to a mean of 0 and
    std of 1."""
    warnings.filterwarnings(action='ignore', category=DataConversionWarning)
    scl = StandardScaler()  # TODO?: try RobustScaler
    numeric_cols = [col for col in X_train if is_numeric(col)]
    if len(X_train):
        X_train.loc[:,numeric_cols] = scl.fit_transform(X_train[numeric_cols])
    else:
        raise ValueError("No training data to fit scaler!")
    if len(X_test):
        X_test.loc[:,numeric_cols] = scl.transform(X_test[numeric_cols])
    # TODO: what about bools etc?

def onehot_subjectid(X_train, X_test):
    sids = list(set(list(X_train['subject_id'].unique()) + list(X_test['subject_id'].unique())))
    to_update = [X_train, X_test]
    for X in to_update:
        for sid in sids:
            sid_str = get_sid_str(sid)
            X.loc[:,'subject_'+sid_str] = X['subject_id']==sid

def get_sid_str(sid):
    sid_str = str(int(sid)) if isinstance(sid, numbers.Number) else sid
    return sid_str

def prep_data(train_data, test_data, train_labels, test_labels, do_pca=True):
    """Scale, impute, and otherwise transform training and testing data, and return
    X,y arrays to be used for ML.
    """
    if list(train_data.columns.values) != list(test_data.columns.values):
        raise ValueError("Train and test set must have same features!")
    X_train = train_data[~pd.isnull(train_labels)].copy()
    y_train = train_labels[~pd.isnull(train_labels)].copy()
    X_test  = test_data.copy()
    y_test  = test_labels.copy()
    onehot_subjectid(X_train, X_test)
    impute(X_train, X_test)
    X_train.drop(columns=['subject_id','measurement_id'], inplace=True, errors='ignore')
    X_test.drop(columns=['subject_id','measurement_id'], inplace=True, errors='ignore')
    if do_pca:
        make_updrs_pca_cols(X_train, X_test)
    scale(X_train, X_test)
    return X_train, X_test, y_train, y_test

############################## Feature selection: ##############################

def make_updrs_pca_cols(df, df2=None, thresh=0.01):
    """Replaces (some) UPDRS columns in df/df2 with PCA versions.  PCA
    will be fit on df.  Both df and df2 (if given) will be updated
    based on this fit.
    """
    if len(df2) == 0:
        warnings.warn("test set is empty; won't attempt to update it with PCA.")
        df2 = None
    updrs_cols = [col for col in df.columns if col.startswith('UPDRS_3')]
    updrs_df = df[updrs_cols]
    pca = PCA(
        n_components = None,
        # whiten = False,
    )
    pca.fit(updrs_df)
    for df_to_update in [df, df2]:
        if df_to_update is None or len(df_to_update) == 0:
            continue
        #print(df2.columns, df2.shape)  # debugging
        pca_output = pca.transform(df_to_update[updrs_cols])
        for cnum,pct in enumerate(pca.explained_variance_ratio_):
            if pct >= thresh:
                df_to_update['UPDRS_3_PCA_%d' % cnum] = pca_output[:,cnum]
        df_to_update.drop(columns=updrs_cols, inplace=True)

        # if len(df2) == 0:
        #     # TODO: if len(df2) == 0: just update column names
        #     todo update cols
        #     continue

        
def get_top_features(X_train, y_train, mode='rfe', min_ft=20, clf_mode='regression', clf=None):
    if clf is not None:
        clf_mode = get_mode(clf)
    if clf_mode == 'regression':
        rfecv_clf = RandomForestRegressor
        kbest_scorer = mutual_info_regression
    elif clf_mode == 'classification':
        rfecv_clf = RandomForestClassifier
        kbest_scorer = mutual_info_classif
    else:
        raise ValueError("Invalid clf mode.")
    print("Running feature selection...")  # debugging
    if mode == 'rfe':
        selector = RFECV(
            rfecv_clf(max_depth=15, n_jobs=8),
            step = 5,
            min_features_to_select = min_ft,
            # scoring = None,  # TODO?  e.g. MSE vs. R^2
            n_jobs = 2
        )
    elif mode == 'kbest':
        selector = SelectKBest(
            kbest_scorer,  # TODO: n_neighbors?
            # f_regression tends to eliminate sensor data
            k = min_ft
        )
    else:
        raise ValueError("Unknown feature reduction mode.")
    selector.fit(X_train, y_train)
    #print(selector.scores_)  # debugging
    features_to_keep = X_train.columns[selector.get_support()]
    print("Will keep %d features." % len(features_to_keep))  # debugging
    return features_to_keep

############################## Machine learning: ###############################

def print_ft_importances(clf, X_test, num=10, logfile=None):
    imp = clf.feature_importances_
    indexes = np.argsort(imp)
    for idx,i in enumerate(reversed(indexes)):
        imp_str = "%s: %0.4f" % (X_test.columns[i], imp[i])
        if logfile is not None:
            with open(logfile,'a') as f:
                f.write("%s\n" % imp_str)
        else:
            print(imp_str)
        if idx>=num-1: break

def predict_probas(clf, X_val):
    if hasattr(clf,'predict_proba'):  # RF, KNN
        y_prob_pred = clf.predict_proba(X_val)
        y_prob_pred_pos = y_prob_pred[:,1]
    elif hasattr(clf,'decision_function'):  # SVM
        y_prob_pred_pos = clf.decision_function(X_val)
    else:
        raise AttributeError("Don't know how to get scores from classifier.")
    return y_prob_pred_pos

def get_roc_values(clf, X_val, y_val):
    y_prob_pred_pos = predict_probas(clf, X_val)
    fpr, tpr, threshes = roc_curve(y_val, y_prob_pred_pos, pos_label=True)  # drop_intermediate=True
    auc = roc_auc_score(y_val, y_prob_pred_pos)  # TODO?: average=...
    return fpr, tpr, threshes, auc

def make_roc_plot(clf, X_val, y_val, title, X_train=None, y_train=None):
    """clf must already be trained."""
    plt.figure()
    plt.plot([0, 1], [0, 1], 'k--')
    if X_train is not None and y_train is not None:
        fpr, tpr, threshes, auc = get_roc_values(clf, X_train, y_train)
        plt.plot(fpr, tpr, label='%s (training) (AUC %0.3f)'%(title,auc))
    fpr, tpr, threshes, auc = get_roc_values(clf, X_val, y_val)
    plt.plot(fpr, tpr, label='%s (testing) (AUC %0.3f)'%(title,auc))
    plt.xlim(0,1)
    plt.ylim(0,1)
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve(s)')
    plt.legend(loc='best')
    plt.show()

def make_error_boxplot(y_predicted_test, y_test, featurename,
                       y_predicted_train=None, y_train=None,
                       ylims=None, filename=None):
    y_predicted_test = y_predicted_test.copy()
    y_test = y_test.copy()
    y_predicted_test = y_predicted_test[~pd.isnull(y_test)]
    y_test = y_test[~pd.isnull(y_test)]
    to_plot = [[y_predicted_test, y_test]]
    titles = ["testing"]
    if y_predicted_train is not None and y_train is not None:
        fig, axes = plt.subplots(1, 2, figsize=(12, 6))
        to_plot.append([y_predicted_train, y_train])
        titles.append("training")
    else:
        fig = plt.figure()
        axes = [plt.gca()]
    for i, (pred, actual) in enumerate(to_plot):
        ax = axes[i]
        possible_vals = sorted(actual.unique())
        boxes = []
        for val in possible_vals:
            boxes.append([p for idx,p in enumerate(pred) if actual.iloc[idx]==val])
        ax.boxplot(boxes, positions=possible_vals)  # TODO?: whis param
        if ylims is not None:
            ax.set_ylim(ylims)
        ax.set_xlabel('%s (reported)' % featurename)
        ax.set_ylabel('%s (predicted)' % featurename)
        ax.set_title(titles[i])
    if filename is None:
        plt.show()
    else:
        plt.savefig(filename)

def print_search_results(clf, test='R^2', logfile=None, show_summary=True):
    """clf is a grid/random search result, not a single classifier."""
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        grid_search_results = pd.DataFrame(clf.cv_results_)
    params_columns = [col for col in grid_search_results.columns if col.startswith('param_')]
    print("Search results (top 50):")
    with pd.option_context('display.precision', 3, 'display.expand_frame_repr', False, 'display.max_rows', None):
        if is_regressor(clf):
            search_result = grid_search_results[['rank_test_%s'%test, 'mean_test_%s'%test, 'std_test_%s'%test] + params_columns].sort_values(by=['rank_test_%s'%test])[:50]
        else:
            search_result = grid_search_results[['rank_test_score', 'mean_test_score', 'std_test_score'] + params_columns].sort_values(by=['rank_test_score'])[:50]
        print(search_result)
        if logfile is not None:
            with open(logfile,'a') as f:
                f.write("%s\n\n" % str(search_result))
    if show_summary:
        print("Best estimator used params:\n%s" % clf.best_params_)
        print("and got score (CV): %f" % clf.best_score_)

def do_search(base_clf, parameters, X_train, y_train, random=False,
              n_iter=10, n_jobs=4, test='R^2', scorer=None,
              logfile=None, show_summary=True):
    """if random is False, parameters will be a dict with lists of values
    to use for each parameter.  if random is True, parameters will be
    a dict with either lists or distribution of values for each
    parameter.  scorer is a dict for scorer(s) to add.
    """
    if is_regressor(base_clf):
        scoring = {
            'R^2': 'r2',
            '-mean_sq_err': 'neg_mean_squared_error',
            #'-mean_abs_err': 'neg_mean_absolute_error',
            #'-med_abs_err': 'neg_median_absolute_error'
        }
        #refit = '-mean_abs_err'
        #refit = 'R^2'
        refit = test
    else:
        scoring = None
        refit = True
    if scorer is not None:
        for k in scorer:
            scoring[k] = scorer[k]
        refit = test
    if not random:
        clf = GridSearchCV(base_clf, parameters, cv=5, verbose=3, n_jobs=n_jobs,
                           scoring=scoring, refit=refit)
    else:
        clf = RandomizedSearchCV(base_clf, parameters, cv=5, verbose=3,
                                 n_jobs=n_jobs, n_iter=n_iter, scoring=scoring,
                                 refit=refit)
    clf.fit(X_train, y_train)
    for test in scoring:  # print grid search results ranked several different ways
        # TODO: refit ...
        print_search_results(clf, test=test, logfile=logfile, show_summary=show_summary)
    return clf.best_estimator_  # TODO?: refit/return for each scoring method

def get_mode(clf):
    if is_classifier(clf):
        mode = 'classification'
    elif is_regressor(clf):
        mode = 'regression'
    else:
        raise TypeError("don't know what clf is.")
    return mode

################################################################################
